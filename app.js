'use strict'

const reekoh = require('reekoh')
const plugin = new reekoh.plugins.Storage()

const moment = require('moment')
const oracledb = require('oracledb')
const isNil = require('lodash.isnil')
const isEmpty = require('lodash.isempty')
const isNumber = require('lodash.isnumber')
const isString = require('lodash.isstring')
const isError = require('lodash.iserror')
const isPlainObject = require('lodash.isplainobject')
const async = require('async-bluebird')
const BPromise = require('bluebird')

let safeParse = BPromise.method(JSON.parse)
BPromise.promisifyAll(oracledb)

let pool = null
let tableName = null
let fieldMapping = null
let parsedMapping = null

let smartLog = log => {
  if (isError(log)) {
    plugin.logException(log)
  } else {
    plugin.log(log)
  }
  console.log(log)
}

let insertData = (data, callback) => {
  let query = `insert into ${tableName} (${data.columns.join(', ')}) values (${data.values.join(', ')})`
  pool.getConnection((connectionError, connection) => {
    if (connectionError) return callback(connectionError)
    connection.execute(query, data.data, { autoCommit: true }, (insertError) => {
      connection.release(() => {
        callback(insertError)
      })
    })
  })
}

let processData = (data, callback) => {
  let keyCount = 0
  let processedData = {
    columns: [],
    values: [],
    data: {}
  }

  async.forEachOf(parsedMapping, (field, key, done) => {
    keyCount++

    processedData.columns.push(`"${key}"`)
    processedData.values.push(`:val${keyCount}`)

    let datum = data[field.source_field]
    let processedDatum

    if (!isNil(datum) && !isEmpty(field.data_type)) {
      try {
        if (field.data_type === 'String') {
          if (isPlainObject(datum)) {
            processedDatum = JSON.stringify(datum)
          } else {
            processedDatum = `${datum}`
          }
        } else if (field.data_type === 'Integer') {
          if (isNumber(datum)) {
            processedDatum = datum
          } else {
            let intData = parseInt(datum)

            if (isNaN(intData)) {
              processedDatum = datum
            } else { // store original value
              processedDatum = intData
            }
          }
        } else if (field.data_type === 'Float') {
          if (isNumber(datum)) {
            processedDatum = datum
          } else {
            let floatData = parseFloat(datum)

            if (isNaN(floatData)) {
              processedDatum = datum
            } else {
              processedDatum = floatData
            }
          }
        } else if (field.data_type === 'Boolean') {
          if ((isString(datum) && datum.toLowerCase() === 'true') || (isNumber(datum) && datum === 1)) {
            processedDatum = 1
          } else if ((isString(datum) && datum.toLowerCase() === 'false') || (isNumber(datum) && datum === 0)) {
            processedDatum = 0
          } else {
            if (datum) {
              processedDatum = 1
            } else {
              processedDatum = 0
            }
          }
        } else if (field.data_type === 'Date' || field.data_type === 'Timestamp') {
          if (isEmpty(field.format) && moment(datum).isValid()) {
            processedDatum = moment(datum).toDate()
          } else if (!isEmpty(field.format) && moment(datum, field.format).isValid()) {
            processedDatum = moment(datum, field.format).toDate()
          } else if (!isEmpty(field.format) && moment(datum).isValid()) {
            processedDatum = moment(datum).toDate()
          } else { processedDatum = datum }
        }
      } catch (e) {
        if (isPlainObject(datum)) { processedDatum = JSON.stringify(datum) } else {
          processedDatum = datum
        }
      }
    } else if (!isNil(datum) && isEmpty(field.data_type)) {
      if (isPlainObject(datum)) {
        processedDatum = JSON.stringify(datum)
      } else { processedDatum = `${datum}` }
    } else {
      processedDatum = null
    }

    processedData.data[`val${keyCount}`] = processedDatum

    done()
  }, () => {
    callback(null, processedData)
  })
    .catch(console.log)
}

plugin.on('data', (data) => {
  if (isPlainObject(data)) {
    processData(data, (error, processedData) => {
      if (error) return smartLog(error)

      insertData(processedData, (error) => {
        if (error) return smartLog(error)

        plugin.emit('processed')
        smartLog(JSON.stringify({
          title: 'Record Successfully inserted to Oracle Database.',
          data: data
        }))
      })
    })
  } else if (Array.isArray(data)) {
    BPromise.each(data, (datum) => {
      processData(datum, (error, processedData) => {
        if (error) return BPromise.reject(error)
        insertData(processedData, (error) => {
          if (error) return BPromise.reject(error)
          plugin.emit('processed')
          return plugin.log({
            title: 'Record Successfully inserted to Oracle Database.',
            data: datum
          })
        })
      })
    }).catch(plugin.logException)
  } else {
    smartLog(new Error(`Invalid data received. Data must be a valid Array/JSON Object or a collection of objects. Data: ${data}`))
  }
})

plugin.once('ready', () => {
  let options = plugin.config

  fieldMapping = options.fieldMapping

  if (options.schema) {
    tableName = `"${options.schema}"."${options.table}"`
  } else {
    tableName = `"${options.table}"`
  }

  safeParse(fieldMapping || '{}')
    .then((parsed) => {
      parsedMapping = parsed
      async.forEachOf(parsedMapping, (field, key, done) => {
        if (isEmpty(field.source_field)) {
          done(new Error('Source field is missing for ' + key + ' in field mapping.'))
        } else if (field.data_type && (field.data_type !== 'String' &&
          field.data_type !== 'Integer' && field.data_type !== 'Float' &&
          field.data_type !== 'Boolean' && field.data_type !== 'Timestamp' &&
          field.data_type !== 'Date')) {
          done(new Error('Invalid Data Type for ' + key + ' in field mapping. Allowed data types are String, Integer, Float, Boolean, Timestamp and Date.'))
        } else {
          done()
        }
      })
    })
    .then(() => {
      oracledb.createPool({
        user: options.user,
        password: options.password,
        connectString: options.connection
      })
        .then((connectionPool) => {
          pool = connectionPool
          smartLog('Connected to Oracle Database Server.')
          plugin.emit('init')
        })
    })
    .catch(smartLog)
})

module.exports = plugin

